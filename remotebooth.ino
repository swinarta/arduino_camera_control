int ledPin = 13; // choose the pin for the LED
int inPin3 = 7;
int inPin2 = 6;
int inPin1 = 5;
int val = 0;
int cameraPin = 8;
int delayBeforeCapture = 100;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);  // declare LED as output
//  pinMode(inPin3, INPUT);
  pinMode(inPin2, INPUT);
//  pinMode(inPin1, INPUT);
  pinMode(cameraPin, OUTPUT);
  digitalWrite(cameraPin, HIGH);
}

void loop(){

  /*
  val = digitalRead(inPin3);
  if (val == HIGH) {
    digitalWrite(ledPin, HIGH);
    Serial.println("3");
    delay(3400);
    camera_on();
  } else {
    digitalWrite(ledPin, LOW);
  }
  */

  val = digitalRead(inPin2);
  if (val == HIGH) {
    digitalWrite(ledPin, HIGH);
    Serial.println("2");
    delay(3400);
    camera_on();
  } else {
    digitalWrite(ledPin, LOW);
  }

/*
  val = digitalRead(inPin1);  
  if (val == HIGH) {  
    digitalWrite(ledPin, HIGH);
    Serial.println("1");
    delay(1400);
    camera_on();    
  } else {
    digitalWrite(ledPin, LOW);
  }
*/  
}

void camera_on(){
  digitalWrite(cameraPin, LOW);
  delay(300);
  digitalWrite(cameraPin, HIGH);
}

